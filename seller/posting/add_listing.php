<?php

require "../../initdb.php";
$msg = "Please enter all details to add listing";
$code = 404;
$data = array();
$errors = array();
$ppdir = "./listing/";
if (isset($_POST[$param_username]) && isset($_POST[$param_pass]) && isset($_POST[$param_email])) {
    $profilepic = $ppdir . "default.png";
    if (!file_exists($ppdir)) {
        mkdir($ppdir);
    }
    while(file_exists($profilepic))
    if ($_FILES[$param_profilepic]) {
        $profilepic = $ppdir . time() . $_FILES[$param_profilepic]["ext"];
        move_uploaded_file($_FILES[$param_profilepic]["name"], $profilepic);
    }
    $query = "insert into seller_login(username,email,password,profilepic) values(?,?,?,?);";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('ssss', $_POST[$param_username], $_POST[$param_email], md5($_POST[$param_pass]), $profilepic);
    if ($stmt->execute()) {
        $msg = "Registration Successful";
        $code = 200;
    } else {
        $msg = "Unable to execute query";
        $code = 500;
    }
    $errors = mysqli_stmt_error_list($stmt);
}
echo json_encode(array(code => $code, msg => $msg, data => $data, errors => $errors));
?>