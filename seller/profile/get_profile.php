<?php

require "../../initdb.php";
$msg = "Please send seller id";
$code = 400;
$data = array();
$errors = array();
if (isset($_POST[$param_id])) {
    $query = "select name,address,city,country,zipcode,phone,cell,cnic from seller_profile where sid = ?;";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('i', $_POST[$param_id]);
    if ($stmt->execute()) {
        $stmt->bind_result($name, $address, $city, $country, $zip, $phone, $cell, $cnic);
        $stmt->fetch();
        if ($stmt->num_rows > 0) {
            $msg = "Profile Found";
            $code = 200;
            $data = array(name => $name, address => $address, city => $city,
            country => $country, zip => $zip, phone => $phone,
            cell => $cell, cnic => $cnic);
        }
        else{
            $msg = "Profile Not Found";
            $code = 404;
        }
    } else {
        $msg = "Unable to execute query";
        $code = 500;
    }
    $errors = mysqli_stmt_error_list($stmt);
}
echo json_encode(array(code => $code, msg => $msg, data => $data, errors => $errors));
?>