<?php

require "../../initdb.php";
$msg = "Please enter both username and password to continue";
$code = 400;
$errors = array();
$data = array();
if (isset($_POST[$param_username]) && isset($_POST[$param_pass])) {
    $query = "select username,email,profilepic from seller_login where (username =? or email=?) and password=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('sss', $_POST[$param_username], $_POST[$param_username], md5($_POST[$param_pass]));
    if ($stmt->execute()) {
        if ($stmt->fetch()) {
            $stmt->bind_result($uname, $email, $profilepic);
            $data = array(username => $uname, email => $email, profilepic => $profilepic);
        } else {
            $msg = "No such user found";
            $code = 200;
        }
    } else {
        $msg = "Unable to execute query";
        $code = 500;
    }
    $errors = mysqli_stmt_error_list($stmt);
}
echo json_encode(array(code => $code, msg => $msg, data => $data));
?>