<?php

require "../../initdb.php";
$msg = "Please enter user id to update";
$code = 404;
$data = array();
$errors = array();
if (isset($_POST[$param_id])) {
    $query = "select name,address,city,country,zipcode,phone,cell,cnic from seller_profile where sid = ?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('i', $_POST[$param_id]);
    if ($stmt->execute()) {
        $stmt->bind_result($name, $address, $city, $country, $zip, $phone, $cell, $cnic);
        $stmt->fetch();
        $name = isset($_POST[$param_name]) ? $_POST[$param_name] : $name;
        $address = isset($_POST[$param_address]) ? $_POST[$param_address] : $address;
        $city = isset($_POST[$param_city]) ? $_POST[$param_city] : $city;
        $country = isset($_POST[$param_country]) ? $_POST[$param_country] : $country;
        $zip = isset($_POST[$param_zipcode]) ? $_POST[$param_zipcode] : $zip;
        $phone = isset($_POST[$param_phone]) ? $_POST[$param_phone] : $phone;
        $cell = isset($_POST[$param_cell]) ? $_POST[$param_cell] : $cell;
        $cnic = isset($_POST[$param_cnic]) ? $_POST[$param_name] : $cnic;
        $stmt->close();
        $query = "Replace into seller_profile (sid,name,address,city,country,zipcode,phone,cell,cnic) VALUES (?,?,?,?,?,?,?,?,?)";
        $stmt = $conn->prepare($query);
        $stmt->bind_param("issssisss", $_POST[$param_id], $name, $address, $city, $country, $zip, $phone, $cell, $cnic);
        if ($stmt->execute()) {
            $msg = "Profile Updated Successfully";
            $code = 200;
        } else {
            $msg = "Profile Update unsuccessful";
            $code = 200;
        }
    } else {
        $msg = "Unable to execute query";
        $code = 500;
    }
    $errors = mysqli_stmt_error_list($stmt);
}
echo json_encode(array(code => $code, msg => $msg, data => $data, errors => $errors));
?>