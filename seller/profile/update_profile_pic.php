<?php

require "../../initdb.php";
$msg = "Please enter all details to update profile";
$code = 400;
$data = array();
$errors = array();
$ppdir = "./seller_pp/";
if (isset($_POST[$param_profilepic]) && isset($_POST[$param_id])) {
    $profilepic = $ppdir . "default.png";
    if (!file_exists($ppdir)) {
        mkdir($ppdir);
    }
    if ($_FILES[$param_profilepic]) {
        $profilepic = $ppdir . time() . $_FILES[$param_profilepic]["ext"];
        move_uploaded_file($_FILES[$param_profilepic]["name"], $profilepic);
    }
    $query = "Update seller_login set profilepic=? where id=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('si', $profilepic, $_POST[$param_id]);
    if ($stmt->execute()) {
        $msg = "Profile pic updated successfully";
        $code = 200;
    } else {
        $msg = "Unable to execute query";
        $code = 500;
    }
    $errors = mysqli_stmt_error_list($stmt);
}
echo json_encode(array(code => $code, msg => $msg, data => $data, errors => $errors));
?>